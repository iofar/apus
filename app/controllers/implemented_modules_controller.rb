class ImplementedModulesController < ApplicationController

def new
end

  def create
      @implementedModule = ImplementedModule.new(implementation_params)
      @implementedModule.implementation_id = params[:implementation_id]
      @implementedModule.save
      redirect_to action: 'new'
  end

  private
	def implementation_params
	    params.require(:implemented_module).permit(:implementedModule,:implementation_id)
end

end
