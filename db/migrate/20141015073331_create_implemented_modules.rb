class CreateImplementedModules < ActiveRecord::Migration
  def change

    create_table :implemented_modules do |t|
      t.string :implementedModule
      t.references :implementation, index: true

      t.timestamps
    end
    #execute "ALTER TABLE implemented_modules ADD PRIMARY KEY (id);"
  end
end
