class CreateImplementations < ActiveRecord::Migration
  def change

    create_table :implementations do |t|
      t.string :client
      t.string :place
      t.string :date

      t.timestamps
    end
    #execute "ALTER TABLE implementations ADD PRIMARY KEY (id);"
  end
end
